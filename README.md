# gainOptimizer

An easy-to-use SDR gain optimization tool for the AcubeSAT Ground Station for quantization and ADC clipping probability reduction. Also supports artificial noise source and narrowband RFI simulation channels for testing purposes.

![GRC Flowgraph](https://i.imgur.com/pCmPlHK.png)
![GUI Screenshot](https://i.imgur.com/ZenuVCm.png)